from sim_utils.replication import Replicator
from sim_utils.parameters_200417 import Scenario


# Set up a dictionary to hold scenarios
scenarios = {}

# Name & define all scenarios (set parameters that differ from defaults in sim_utils/parameters.py)

scenarios['30k_recommended'] = Scenario(samples_per_day = 30000,
                                resource_numbers = {
                                'human_sample_receipt': 12,
                                'human_sample_prep': 5,
                                'human_rna_prep': 4,
                                'human_pcr': 3,
                                'biomek': 16,
                                'pcr_plate_stamper': 2,
                                'pcr_plate_reader': 13,
                                'sample_prep_automation': 5
                                },
                                workstation_capacity = {
                                'workstation_0': 99999,
                                'workstation_1_man': 12,
                                'workstation_1_auto': 5, 
                                'workstation_2': 16,
                                'workstation_3': 2,
                                'workstation_4': 13
                                })


# Set up and call replicator
replications = 30
replications = Replicator(scenarios, replications)
replications.run_scenarios()