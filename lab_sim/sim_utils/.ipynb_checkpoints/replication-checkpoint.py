import inspect
import numpy as np
import pandas as pd
from joblib import Parallel, delayed
from sim_utils.helper_functions import expand_multi_index
from sim_utils.model import Model


class Replicator:
    
    def __init__(self, scenarios, replications):
        
        self.replications = replications
        self.scenarios = scenarios
        
        # Set up DataFrames for all trials results
        self.summary_output = pd.DataFrame()
        self.summary_output_by_day = pd.DataFrame()
        self.summary_queue_times = pd.DataFrame()
        self.summary_resources = pd.DataFrame()
        self.summary_relative_queues = pd.DataFrame()
        self.output_pivot = pd.DataFrame()
        self.resources_pivot = pd.DataFrame()
        
            
    def pivot_results(self):   
        
        # Define functions for pivot table (only accessible in this method)
        
        def q1(g):
            return np.percentile(g, 25)

        def q3(g):
            return np.percentile(g, 75)
        
        # Output summary
        df = self.summary_output.copy()
        df['result_type'] = df.index
         
        pivot = df.pivot_table(
            index = ['result_type', 'name'],
            values = ['Result'],
            aggfunc = [np.min, np.median, np.max],
            margins=False)
        
        self.output_pivot = pivot
        
        # resource results
        df = self.summary_resources.copy()
        cols = list(df)
        cols.remove('run')
        df['resource'] = df.index
        
        self.resources_pivot = df.pivot_table(
            index = ['resource', 'name'],
            values = ['used', 'capacity', 'utilisation'],
            aggfunc = [np.min, np.median, np.max],
            margins=False)
        
        # Queue time results
        
        df = self.summary_queue_times.copy()
         
        pivot = df.pivot_table(
            index = ['queue', 'name'],
            values = ['median', '95_percent'],
            aggfunc = [np.min, np.median, np.max],
            margins=False)

        self.queue_times_median_pivot = df.pivot_table(
            index = ['queue', 'name'],
            values = ['median'],
            aggfunc = [np.min, np.median, np.max],
            margins=False)

        self.queue_times_95_percent_pivot = df.pivot_table(
            index = ['queue', 'name'],
            values = ['95_percent'],
            aggfunc = [np.min, np.median, np.max],
            margins=False)
     
 
    def print_results(self):
                
        print('\nOutput results')
        print('--------------')
        print(self.output_pivot)
        print()
        print('\n\nMedian resources used')
        print('---------------------')
        print(self.resources_pivot['median'])
        print()
        print('Median queuing times')
        print('--------------------')
        print(self.queue_times_median_pivot)
        print()
        print('95th percentile queuing times')
        print('-----------------------------')
        print(self.queue_times_95_percent_pivot)

        
    
    def run_trial(self, scenario):
        trial_output = Parallel(n_jobs=-1)(delayed(self.single_run)(scenario, i) 
                for i in range(self.replications))
        
        return trial_output
        
    
    def save_results(self):
        
        self.summary_output.to_csv('./output/output.csv')
        self.summary_output_by_day.to_csv('./output/output_by_day.csv')
        self.summary_resources.to_csv('./output/resources.csv')
        self.summary_relative_queues.to_csv('./output/queues.csv')
        self.output_pivot.to_csv('./output/output_summary.csv')
        self.resources_pivot.to_csv('./output/resouces.summary.csv')
    
    
    def single_run(self, scenario, i=0):
        print(f'{i}, ', end='' )
        model = Model(scenario)
        model.run()
        
        # Put results in a dictionary
        results = {
            'output': model.process.audit.summary_output,
            'output_by_day': model.process.audit.summary_output_by_day,
            'resources': model.process.audit.summary_resources,
            'relative_queues': model.process.audit.relative_queues,
            'queue_times': model.process.audit.queue_times
                   }
        
        return results
        
    
    def run_scenarios(self):
        
        # Run all scenarios
        scenario_count = len(self.scenarios)
        counter = 0
        for name, scenario in self.scenarios.items():
            counter += 1
            print(f'\r>> Running scenario {counter} of {scenario_count}', end='')
            scenario_output = self.run_trial(scenario)
            self.unpack_trial_results(name, scenario_output)
        
        # Clear progress output
        clear_line = '\r' + " " * 79
        print(clear_line, end = '')
        
        # Pivot results
        self.pivot_results()
        
        # Print results
        self.print_results()
        
        # save results
        self.save_results()
        


    def unpack_trial_results(self, name, results):
        

        for run in range(self.replications):
            
            # Output summary
            result_item = results[run]['output']
            result_item['run'] = run
            result_item['name'] = name
            self.summary_output = self.summary_output.append(result_item)
            
            # Output by day summary
            result_item = results[run]['output_by_day']
            result_item['run'] = run
            result_item['name'] = name
            self.summary_output_by_day = self.summary_output_by_day.append(result_item)
            
            # Resources summary
            result_item = results[run]['resources']
            result_item['run'] = run
            result_item['name'] = name
            self.summary_resources = self.summary_resources.append(result_item)
            
            # Queue summary
            result_item = results[run]['relative_queues']
            result_item['run'] = run
            result_item['name'] = name
            self.summary_relative_queues = self.summary_relative_queues.append(result_item)
            
            # Queueing time summary
            result_item = results[run]['queue_times']
            result_item['run'] = run
            result_item['name'] = name
            self.summary_queue_times = self.summary_queue_times.append(result_item)
            

            
        
    