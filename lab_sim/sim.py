from sim_utils.model import Model
from sim_utils.parameters_200417 import Scenario

# Temporary run model without replication

scenario = Scenario()
model = Model(scenario)
model.run()
